#include <OneWire.h>
#include <DallasTemperature.h>
#include <SoftwareSerial.h>
#include <EEPROM.h>
#include "watheat.h"

// This Arduino sketch manages a solar water heater
// It controls 1 pump, 1 valve, and reads 4 temperature sensors.
// It leverages tutorial:
// http://www.hacktronics.com/Tutorials/arduino-1-wire-tutorial.html
/*
   From left to right 1-Gnd, 2-Signal, 3-power
   parasitic power wiring ties 1 to 3 on DS1820 temp sensor.
   Pin 3 signal Pulled up thru 4.7K resistor to +5v
   Pin in middle of daughterboard, closest to Uno

   Temperatures seem to vary as much as 1 degree F, between multiple sensors placed side by side

   Pin A0 used as voltage sense w/ resistor network of 14.9K and 3.2K
   pin next to DC-DC Converter on daughterboard

   RF serial port uses Pins 10,11 - wires are flipped to daughterboard

   Relay is controlled via transistor, 1K resistor on pin 5 - pin in corner, closest to uno

   Daughterboard requires gnd and +5 from Uno - middle pins next to Uno
*/

#define MAJOR_VERSION 1
#define MINOR_VERSION 1

#define WIRED_SERIAL    0  // 0 for RF, 1 for debugging
#define TTY_DISPLAY     1 // 0 for binary output, 1 for tty output
#define SHUNT200_100    0 // 1 for 200A,100mv shunt, else 50, 50mv shunt
#define SHUNT_AMP       100
//#define SHUNT_AMP       20


// using the external 3DR Power Module sensors
float amult = .0048, vmult = .0048; //.048; //- brings to mvoltage level
float apermv = 15.5; // amps per mv
float vpermv = 10.0; // volts per mv

float volts1, volts2, amps1, amps2, amps3;
float vread;           // variable to store the value read
float aread;

// NVRam
NVRAM nvr;

bool cfgMode;

#if WIRED_SERIAL
#define SERDEV Serial
#else
#define SERDEV mySerial
#endif

// terminal stuff
  int loopTime=LOOP_TIME;
  int state;
  int row;
    
// serial port
SoftwareSerial mySerial(10, 11); // RX, TX

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);



// Assign the addresses of your 1-Wire temp sensors.
// See the tutorial on how to obtain these addresses:
// http://www.hacktronics.com/Tutorials/arduino-1-wire-address-finder.html
/*
   Power via parasitic power - gnd and VCC of the temp sensor are tied together.
   +5 goes thru 4.7K resistor to the data line.  All sensors are done in parallel to this.
*/
/*
   Heater operates as follows:
   3 temperature sensors:
    - water reservoir
    - water collector
    - air collector
   Pump at base of reservoir
   Valve at return to reservoir

   Return valve and pump are operated from one relay.
   Valve exists to block gravity fed, water return
   Water is pumped from reservoir, to a solar collector,
   Pump stops, water is heated by the sun



   if air temp of collector > water temp of reservoir, start process
   open valve, start pump, start timer
   when timer expires, close valve, stop pump
   wait for water temp in collector to equal air temp in collector
   cycle water
   repeat

   Allow for temp sensor variance
   Timing for cycling water is dependent upon length of collector
*/
/*
  DeviceAddress therm1 = { 0x28, 0x8e, 0xb8, 0xc0, 0x03, 0x00, 0x00, 0x4b }; // I
  DeviceAddress therm2 = { 0x28, 0xFF, 0xDA, 0xA6, 0x64, 0x16, 0x03, 0x94 };// II
  DeviceAddress therm3 = { 0x28, 0xFF, 0xFB, 0x72, 0x64, 0x16, 0x03, 0xAE }; // III
  DeviceAddress therm4 = { 0x28, 0xFF, 0x10, 0xAB, 0x64, 0x16, 0x03, 0x96}; // IIII
  DeviceAddress therm5 = { 0x28, 0xFF, 0xE1, 0xB4, 0x64, 0x16, 0x03, 0x5F}; // V
  DeviceAddress therm6 = { 0x28, 0xFF, 0xF2, 0x42, 0x63, 0x16, 0x04, 0xEC}; // VI
*/



int relayState = 0;
float sysVolts = 0;

float temps[NUM_THERM];  // the different temperatures
float thermAdjust[NUM_THERM]; // adjustments to be made between each thermometer
byte therms[NUM_THERM][BYTES_IN_ADDR] = { // N addresses of 8 bytes each
  { 0x28, 0x8e, 0xb8, 0xc0, 0x03, 0x00, 0x00, 0x4b }, // I
  { 0x28, 0xFF, 0xDA, 0xA6, 0x64, 0x16, 0x03, 0x94 },// II
  { 0x28, 0xFF, 0xFB, 0x72, 0x64, 0x16, 0x03, 0xAE }, // III
  { 0x28, 0xFF, 0x10, 0xAB, 0x64, 0x16, 0x03, 0x96}, // IIII
  { 0x28, 0xFF, 0xE1, 0xB4, 0x64, 0x16, 0x03, 0x5F}, // V
  { 0x28, 0xFF, 0xF2, 0x42, 0x63, 0x16, 0x04, 0xEC} // VI
};
byte thermPresent[NUM_THERM];

byte sensorCount;  // number of sensors read from oneWire bus

void printAddr(DeviceAddress addr)
{
  for (byte n = 0; n < 8; n++)
  {
    SERDEV.print(addr[n], HEX);
    SERDEV.print(" ");
  }
}
void chkTherm (DeviceAddress addr)
{
  // chk the list of therms to see if this one was found.
  int i, j;
  for (i = 0; i < NUM_THERM; i++)
  {
    for (j = 0; j < BYTES_IN_ADDR; j++)
    {
      if (addr[j] != therms[i][j])
        break; // move along
    }

    if (j >= BYTES_IN_ADDR)
    {
      // we matched all 8 bytes
      thermPresent[i] = 1;
      SERDEV.print("Matched address ");
      printAddr(addr);
      SERDEV.print(" Therm #");
      SERDEV.print(i + 1); // make it 1 relative
      SERDEV.print("\n\r");
      break;
    }
  }
}


float getTemp(int thermoID)
{
  float tempC, tempF = -127;

  if (thermoID >= 0)
  {
    //thermoID--; // make it 0 relative.
    if (thermPresent[thermoID])
    {
      tempC = sensors.getTempC(therms[thermoID]);
      if (tempC == -127.00)
      {
        SERDEV.print("Error getting temperature");
      }
      else
        tempF = DallasTemperature::toFahrenheit(tempC);
    }
    //      else
    //          SERDEV.println("Thermometer not present");
  }
  return tempF;
}
float printTemperature(DeviceAddress deviceAddress)
{
  float tempF, tempC = sensors.getTempC(deviceAddress);
  if (tempC == -127.00) {
    SERDEV.print("Error getting temperature");
  } else {
    SERDEV.print("C: ");
    SERDEV.print(tempC);
    SERDEV.print(" F: ");
    tempF = DallasTemperature::toFahrenheit(tempC);
    SERDEV.print(tempF);
  }
  return tempF;
}

void getTemps(void)
{
  int i;
  float temp;
  sensors.requestTemperatures();
  //SERDEV.print("Getting temperatures...\n\r");

  for (i = 0; i < NUM_THERM; i++)
  {
    temps[i] = getTemp(i);
  }
}

void get3drPower()
{
  /*
   * float amult = .0048, vmult = .0048; //.048; //- brings to mvoltage level
   * float apermv = 15.5; // amps per mv
   * float vpermv = 10.0; // volts per mv
   */
  delay(10);
  vread = analogRead(VOLT1_PIN);   // read the input pin
  delay(10);
  aread = analogRead(CUR1_PIN);
  delay(10);
#if 0
  volts1 = vmult * vread;
  volts1 = volts1 * vpermv;
  amps1 = aread * amult;
  amps1 = amps1 * apermv;
#else
  volts1 = vread*nvr.nvrVoltMult[0];
  amps1 = aread * nvr.nvrAmpMult[0];
#endif

  vread = analogRead(VOLT2_PIN);   // read the input pin
  delay(10);
  aread = analogRead(CUR2_PIN);
  delay(10);
#if 0
  volts2 = vmult * vread;
  volts2 = volts2 * vpermv;
  amps2 = aread * amult;
  amps2 = amps2 * apermv;
#else
  volts2 = vread*nvr.nvrVoltMult[1];
  amps2 = aread * nvr.nvrAmpMult[1];
#endif
}
float getVoltage(int pin)
{
  delay(10);
  int analog_value = analogRead(pin);
  delay(10);
  //SERDEV.print("analogread: ");
  //SERDEV.println(analog_value);
  float temp, temp1 = (analog_value * 5.0) / 1024.0;
  //SERDEV.print("analogvalue: ");
  //SERDEV.println(temp1);
  temp = temp1 / (RES2 / (RES1 + RES2));

  if (temp < 0.1)
  {
    temp = 0.0;
  }
  return temp;
}
void getShuntPower()
{
  analogRead(CUR3_PIN);
  delay(10);
  analogRead(CUR3_PIN);
  delay(10);
  amps3 = aread = analogRead(CUR3_PIN);
  delay(10);
#if 1
// ina198 + 200/100 shunt
#if SHUNT200_100
  amps3=aread*.108; // ina198 + 200/100 shunt
#else
  amps3=aread*nvr.nvrAmpMult[2]; // ina198 + 50/50 shunt
#endif
  amps3+=nvr.nvrAmpAdd[2];
#endif
#if 0
  if (amps3 > 0)
#if SHUNT200_100
    amps3 = amps3 * .5 + 1; // function for 200a/100mv shunt w/ ina193
#else
    amps3 = amps3 * .3 + 1; // function for 200a/100mv shunt w/ ina193
#endif
#endif
}

void setRelay(void)
{
  if (relayState == 1)
    digitalWrite(RELAY_PIN, HIGH);   // set Relay to state
  else
    digitalWrite(RELAY_PIN, LOW);   // set Relay to state
}
///////////////////////////
//display


void gotoXY(int row, int col)
{
  SERDEV.write(ESCAPE);
  SERDEV.print("[");
  SERDEV.print(row);
  SERDEV.print(";");
  SERDEV.print(col);
  SERDEV.print("H");
}
void ceop()
{
  SERDEV.write(ESCAPE);
  SERDEV.print("[");
  SERDEV.print("J");
}
void showStatBat()
{
    SERDEV.print("CPU Voltage: ");
  //SERDEV.println(sysVolts);
  gotoXY(2, 1);
  SERDEV.print("Primary Battery Voltage: ");
  //SERDEV.println(volts1);
  gotoXY(3, 1);
  SERDEV.print("Secondary Battery Voltage: ");
  //SERDEV.println(volts2);
  gotoXY(4, 1);
  SERDEV.print("Primary Battery Charge Current: ");
  //SERDEV.println(amps1);
  gotoXY(5, 1);
  SERDEV.print("Secondary Battery Charge Current: ");
  //SERDEV.println(amps2);
  gotoXY(6, 1);
  SERDEV.print("Primary Battery Drain Current: ");
  //SERDEV.println(amps3);
  gotoXY(7, 1);
  SERDEV.print("Relay: ");
#if 0
  if (relayState)
    SERDEV.println("On");
  else
    SERDEV.println("Off");
#endif

  // temps
  gotoXY(1, 50);
  SERDEV.print("Attic: ");
  //  SERDEV.println(temps[TEMP_INLET_WATER]);
  gotoXY(2, 50);
  SERDEV.print("Interior: ");
  //  SERDEV.println(temps[TEMP_RESERVOIR]);
  gotoXY(3, 50);
  SERDEV.print("Exterior: ");
  //  SERDEV.println(temps[TEMP_COLL_WATER]);
}

void showStatCfg()
{
      SERDEV.print("Configuration: ");
        gotoXY(2, 1);
        SERDEV.print("Nvram Version: ");
        SERDEV.print(nvr.ver);
        
}
void showStatic()
{
  gotoXY(1, 1);
  ceop();
  if (cfgMode)
    showStatCfg();
  else
    showStatBat();
}
void showDynCfg()
{
  /*
   * typedef struct {
  // This is the memory map for nvram (eeprom)
  byte sig; // shows if we have initialized nvram or not
  byte ver; // version of nvram
  float nvrVoltMult[3];
  byte nvrVoltADD[3];
  float nvrAmpMult[3];
  byte nvrAmpAdd[3];
} NVRAM;
   */
   int i;
   for (i=0;i<NUM_VOLT_SENSOR;i++)
   {
      gotoXY(2+i, 0);
      SERDEV.print("Voltage ");
      SERDEV.print(i+1);
      SERDEV.print(" multiplier: ");
      SERDEV.print(nvr.nvrVoltMult[i]);
   }
   for (i=0;i<NUM_VOLT_SENSOR;i++)
   {
      gotoXY(2+NUM_VOLT_SENSOR+i, 0);
      SERDEV.print("Voltage ");
      SERDEV.print(i+1);
      SERDEV.print(" Add: ");
      SERDEV.print(nvr.nvrVoltAdd[i]);
   }

   for (i=0;i<NUM_CURRENT_SENSOR;i++)
   {
      gotoXY(2+NUM_VOLT_SENSOR+NUM_VOLT_SENSOR+i, 0);
      SERDEV.print("Current ");
      SERDEV.print(i+1);
      SERDEV.print(" multiplier: ");
      SERDEV.print(nvr.nvrAmpMult[i]);
   }

   for (i=0;i<NUM_CURRENT_SENSOR;i++)
   {
      gotoXY(2+NUM_VOLT_SENSOR+NUM_VOLT_SENSOR+NUM_CURRENT_SENSOR+i, 0);
      SERDEV.print("Current ");
      SERDEV.print(i+1);
      SERDEV.print(" Add: ");
      SERDEV.print(nvr.nvrAmpAdd[i]);
   }
   gotoXY(1+row,1);
}
void showDynBat()
{
      
  gotoXY(1, 35);
  //SERDEV.print("CPU Voltage: ");
  SERDEV.print(sysVolts);
  SERDEV.print("    ");
  gotoXY(2, 35);
  //SERDEV.print("Primary Battery Voltage: ");
  SERDEV.print(volts1);
  SERDEV.print("    ");
  gotoXY(3, 35);
  //SERDEV.print("Secondary Battery Voltage: ");
  SERDEV.print(volts2);
  SERDEV.print("    ");
  gotoXY(4, 35);
  //SERDEV.print("Primary Battery Charge Current: ");
  SERDEV.print(amps1);
  SERDEV.print("    ");
  gotoXY(5, 35);
  //SERDEV.print("Secondary Battery Charge Current: ");
  SERDEV.print(amps2);
  SERDEV.print("    ");
  gotoXY(6, 35);
  //SERDEV.print("Primary Battery Drain Current: ");
  SERDEV.print(amps3);
  SERDEV.print("    ");
  gotoXY(7, 35);
  //SERDEV.print("Relay: ");
  if (relayState)
    SERDEV.println("On ");
  else
    SERDEV.println("Off");

  // temps
  gotoXY(1, 60);
  //  SERDEV.print("Attic: ");
  SERDEV.print(temps[TEMP_INLET_WATER]);
  SERDEV.print("    ");
  gotoXY(2, 60);
  //  SERDEV.print("Interior: ");
  SERDEV.print(temps[TEMP_RESERVOIR]);
  SERDEV.print("    ");
  gotoXY(3, 60);
  //  SERDEV.print("Exterior: ");
  SERDEV.print(temps[TEMP_COLL_WATER]);
  SERDEV.print("    ");

}
void showDynamic()
{
  if (cfgMode)
    showDynCfg();
  else
    showDynBat();
}

void printTemps(void)
{
  SERDEV.print("Attic: ");
  SERDEV.println(temps[TEMP_INLET_WATER]);
  SERDEV.print("Interior: ");
  SERDEV.println(temps[TEMP_RESERVOIR]);
  SERDEV.print("Exterior: ");
  SERDEV.println(temps[TEMP_COLL_WATER]);
}
/// NVRam code

void initNvr(void)
{
  /*#define NVR_VOLT_MULT_1 .048
#define NVR_VOLT_MULT_2 .048
#define NVR_VOLT_ADD_1 0
#define NVR_VOLT_ADD_2 0
#define NVR_AMP_MULT_2 (.108/2)
#define NVR_AMP_ADD_2 .43
*/
    nvr.sig=NVRAM_SIG;
    nvr.ver=NVRAM_VER;

    nvr.nvrVoltMult[0]=NVR_VOLT_MULT_0;
    nvr.nvrVoltMult[1]=NVR_VOLT_MULT_1;

    nvr.nvrVoltAdd[0]=NVR_VOLT_ADD_0;
    nvr.nvrVoltAdd[1]=NVR_VOLT_ADD_1;
    
    nvr.nvrAmpMult[0]=NVR_AMP_MULT_0;
    nvr.nvrAmpAdd[0]=NVR_AMP_ADD_0;

    nvr.nvrAmpMult[1]=NVR_AMP_MULT_1;
    nvr.nvrAmpAdd[1]=NVR_AMP_ADD_1;

    nvr.nvrAmpMult[2]=NVR_AMP_MULT_2;
    nvr.nvrAmpAdd[2]=NVR_AMP_ADD_2;

}
void writeNvr(void)
{
    /*
typedef struct {
  // This is the memory map for nvram (eeprom)
  byte sig; // shows if we have initialized nvram or not
  byte ver; // version of nvram
  float nvrVoltMult[3];
  byte nvrVoltADD[3];
  float nvrAmpMult[3];
  byte nvrAmpAdd[3];
} NVRAM;
*/
  EEPROM.put(offsetof(NVRAM,sig), nvr.sig);  
  EEPROM.put(offsetof(NVRAM,ver), nvr.ver);  
  
}
void zeroNvr(void)
{
  int i;
  for (i=0;i<NUM_VOLT_SENSOR;i++)
  {
     nvr.nvrVoltMult[i]=1;
     nvr.nvrVoltAdd[i]=0;
  }
  for (i=0;i<NUM_CURRENT_SENSOR;i++)
  {
     nvr.nvrAmpMult[i]=1;
     nvr.nvrAmpAdd[i]=0;
  }
}
void loadNvram(void)
{
  /*
typedef struct {
  // This is the memory map for nvram (eeprom)
  byte sig; // shows if we have initialized nvram or not
  byte ver; // version of nvram
  float nvrVoltMult[3];
  byte nvrVoltADD[3];
  float nvrAmpMult[3];
  byte nvrAmpAdd[3];
} NVRAM;
  */
  SERDEV.print("offset sig ");
  SERDEV.println(offsetof(NVRAM,sig));
  SERDEV.print("offset nvrvoltmult ");
  SERDEV.println(offsetof(NVRAM,nvrVoltMult[0]));
  SERDEV.print("offset nvrampmult ");
  SERDEV.println(offsetof(NVRAM,nvrAmpMult[0]));
    
  EEPROM.get(offsetof(NVRAM,sig), nvr.sig);
  EEPROM.get(offsetof(NVRAM,ver), nvr.ver);
  if (nvr.sig!=NVRAM_SIG && nvr.ver!=NVRAM_VER)
  {
    // nvram not initialized
    // load from default
    initNvr();
    
  }
  else
  {
    // nvram initialized
  }
}
void adjVar(int idx,float adj)
{
  idx-=1; // 0 based
  if (idx<NUM_VOLT_SENSOR)
  {
    // in the volt multiplier group
    nvr.nvrVoltMult[idx]+=adj;
  }
  else if (idx<2*NUM_VOLT_SENSOR)
  {
    // in the voltage adder group
    idx-=NUM_VOLT_SENSOR;
    nvr.nvrVoltAdd[idx]+=adj;
  }
  else if (idx<(2*NUM_VOLT_SENSOR)+NUM_CURRENT_SENSOR)
  {
    // in the current multiplier group
    idx-=2*NUM_VOLT_SENSOR;
    nvr.nvrAmpMult[idx]+=adj;
  }
  else
  {
    // in the current adder group
    idx-=2*NUM_VOLT_SENSOR+NUM_CURRENT_SENSOR;
    nvr.nvrAmpAdd[idx]+=adj;
  }
}

//////////////////////////////// SETUP
void setup(void)
{
  pinMode(RELAY_PIN, OUTPUT);
  setRelay();

  // start serial port
  // set the data rate for the SoftwareSerial port
  SERDEV.begin(57600);
  SERDEV.println("Solar Heater");

  SERDEV.print("Version ");
  SERDEV.print(MAJOR_VERSION);
  SERDEV.print(".");
  SERDEV.println(MINOR_VERSION);


    loadNvram();

  int i;
  DeviceAddress addr;
//  Serial.begin(57600);
  // Start up the library
  sensors.begin();
  sensorCount = sensors.getDeviceCount();
  SERDEV.print("Sensors found: ");
  SERDEV.println(sensorCount);
  // set the resolution to 10 bit (good enough?)
  oneWire.reset_search();
  for (i = 0; i < sensorCount; i++)
  {

    if (oneWire.search(addr))
    {
      SERDEV.print("Found address ");
      printAddr(addr);
      SERDEV.print("\n\r");
      sensors.setResolution(addr, 10);
      chkTherm(addr);
    }
  }
  cfgMode=false;
  showStatic();
}

void loop(void)
{
  if (SERDEV.available())
  {
    int a = SERDEV.read();
    if (state==0)
      switch (a)
      {
        case '+':
        if (cfgMode==true)
          adjVar(row,.01);
              showDynamic();
        break;

        case '-':
        if (cfgMode==true)
          adjVar(row,-.01);
              showDynamic();
        break;
        
        case 'r':
        case 'R':
          relayState = !relayState; // invert it
          break;
        case 12: //^L
          showStatic();
          break;
        case 'c': //c,C
        case 'C': //c,C // enter config mode
          if (cfgMode==false)
          {
            cfgMode=true;
            row=1;
            state=0;
            showStatic();
            showDynamic();
          }
          break;
        case 'w': //w,W // exit cfg write the nvram parms
        case 'W': //w,W
          cfgMode=false;
          writeNvr();
          showStatic();
          break;
        case 'x': //x,X// exit cfg, reload original nvram
        case 'X': //x,X
          cfgMode=false;
          showStatic();
          showDynamic();
          break;

        case 'i': ////  reload default nvram
        case 'I': //
          initNvr();
          showStatic();
          showDynamic();
          break;

        case 'z': //// exit cfg, reload original nvram
        case 'Z': //z,Z
          zeroNvr();
          showStatic();
          showDynamic();
          break;

       case ESCAPE:
        if (state==0)
        {
          state=1;
          loopTime=1;
        }
        break;
          
        default:
          gotoXY(24,1);
          SERDEV.print("Read char: ");
          SERDEV.print(a);
          state=0;
          loopTime=LOOP_TIME;
      } // switch
    else // state
    {
      if (state==1 && a==BRACKET)
        state=2;
      else if (state==2)
      {
          if (a==CHAR_A)
            if (row>1)
              row--;
          if (a==CHAR_B)
            if (row<MAX_ROW)
             row++;
          state=0;
          loopTime=LOOP_TIME;
          showDynamic();
      }
      else 
      {
          state=0;
          loopTime=LOOP_TIME;
      }
    } // state
  } // if serdev avail

  if (state==0 && cfgMode==false)
  {
    getTemps();
    //printTemps();
    sysVolts = getVoltage(SYS_VOLT_PIN);
    get3drPower(); // reads the volt/current from 2 3dr PM modules
    getShuntPower();
    showDynamic();
    setRelay();
    delay(loopTime);
  }

}
