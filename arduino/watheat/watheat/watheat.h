


int RELAY_PIN = 5;                 // LED connected to digital pin 13

#define NUM_VOLT_SENSOR 3
#define NUM_CURRENT_SENSOR 3
// for the built in power supply/current sensor on the arduino carrier board
#define SYS_VOLT_PIN    A0
#define RES1    14900.0   // divide by resistor network 15K
#define RES2    3240.0    // divide by resistor network 3.2K

#define CUR1_PIN  A1
#define CUR2_PIN  A2

#define VOLT1_PIN  A3
#define VOLT2_PIN  A4

#define CUR3_PIN  A5

// Data wire is plugged into pin 3 on the Arduino
#define ONE_WIRE_BUS 3

#define NUM_THERM   6
#define BYTES_IN_ADDR 8

#define LOOP_TIME 1000 // time between cycle checks - 1 second in milliseconds
#define PUMP_TIME 8 // time to cycle water thru collector in seconds

#define TEMP_VARIATION    1
// temp ID is -1 from numbers on sensor
#define TEMP_COLL_WATER   4 // sensor mounted within water of collector
#define TEMP_COLL_AIR     6 // sensor mounted in air of collector
#define TEMP_INLET_WATER  2 // sensor mounted within water, prior to collector
#define TEMP_RESERVOIR    5 // sensor mounted in/on reservoir

// ascii chars
#define ESCAPE 27
#define BRACKET 91
#define CHAR_A  65
#define CHAR_B  66

#define MAX_ROW 2*NUM_VOLT_SENSOR+2*NUM_CURRENT_SENSOR

// my nvram struct
#define NVRAM_SIG 0x69 // shows that we have initialized nvram
#define NVRAM_VER 3 // 
typedef struct {
  // This is the memory map for nvram (eeprom)
  byte sig; // shows if we have initialized nvram or not
  byte ver; // version of nvram
  float nvrVoltMult[NUM_VOLT_SENSOR];
  float nvrVoltAdd[NUM_VOLT_SENSOR];
  float nvrAmpMult[NUM_CURRENT_SENSOR];
  float nvrAmpAdd[NUM_CURRENT_SENSOR];
} NVRAM;
#define NVR_VOLT_MULT_0 .05 // 3dr
#define NVR_VOLT_MULT_1 .048 // 3dr
#define NVR_VOLT_ADD_0 0
#define NVR_VOLT_ADD_1 0 

#define NVR_AMP_MULT_0 .074 // 3dr
#define NVR_AMP_ADD_0 0 // 
#define NVR_AMP_MULT_1 .074 // 3dr
#define NVR_AMP_ADD_1 0 // 

//#define NVR_AMP_MULT_2 (.108/2) // big shunt
#define NVR_AMP_MULT_2 .045 // big shunt
#define NVR_AMP_ADD_2 .43 // big shunt

